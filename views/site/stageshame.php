<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'WEIRD STAGES';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white comic" Style="margin-bottom: -30px">
        <h1 class="display-4 comic">WEIRD STAGES CATEGORY AWARDS</h1>

        <p class="lead">Ever stop to think, and forget to start again?</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">

            <div class="card text-center  shame-card">
                <?= Html::img("@web/images/B_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Shortest stage Award</h5>
                    <?= Html::a('View', ['etapa/ssa'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <div class="card text-center  shame-card">
                <?= Html::img("@web/images/B_port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Least ports Award</h5>
                    <?= Html::a('View', ['puerto/lpa'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <div class="card text-center  shame-card">
                <?= Html::img("@web/images/small_port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Smallest port Award</h5>
                    <?= Html::a('View', ['puerto/spa'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Print dossier', ['site/print'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Go back', ['site/shame'], ['class' => 'btn btn-danger']) ?>
        </div>

    </div>
</div>