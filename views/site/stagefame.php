<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'CHALLENGING STAGES';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: -30px">
        <h1 class="display-4">CHALLENGING STAGES CATEGORY AWARDS</h1>

        <p class="lead">If it doesn't challenge you, it won't change you.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Longest stage Award</h5>
                    <?= Html::a('View', ['etapa/lsa'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/ports.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Most ports Award</h5>
                    <?= Html::a('View', ['puerto/mpa'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

            <div class="card text-center  fame-card">
                <?= Html::img("@web/images/port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Tallest port Award</h5>
                    <?= Html::a('View', ['puerto/tpa'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Print dossier', ['site/print'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Go back', ['site/index'], ['class' => 'btn btn-warning']) ?>
        </div>

    </div>
</div>