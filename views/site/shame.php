<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'MAD Awards 2022';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white comic" Style="margin-bottom: 0px">
        <h1 class="display-4 comic">HALL OF SHAME</h1>

        <p class="lead">Errors have been made. Others will be blamed.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">
            
            <div class="card center  shame-card">
                <?= Html::img("@web/images/B_cyclist.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Cyclists', ['site/cyclistshame'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
            
            <div class="card center  shame-card">
                <?= Html::img("@web/images/B_team.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Teams', ['site/teamshame'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
            
            <div class="card center  shame-card">
                <?= Html::img("@web/images/B_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Stages', ['site/stageshame'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
            
            <div class="card center  shame-card">
                <?= Html::img("@web/images/B_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Maillots', ['site/maillotshame'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
            
        </div>

        <div class="jumbotron text-right bg-transparent text-white flex-shrink-1" Style="padding-bottom: 1px; margin-bottom: 0px">
            <?= Html::a('Take me back to the Hall of Fame..', ['site/index'], ['class' => 'text-reset lead']) ?>
        </div>

    </div>
</div>