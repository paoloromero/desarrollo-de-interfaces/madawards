<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'EASYEST MAILLOTS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white comic" Style="margin-bottom: -30px">
        <h1 class="display-4 comic">EASYEST MAILLOTS CATEGORY AWARDS</h1>

        <p class="lead">All I ask is the chance to prove that money can't make me happy.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row" Style="justify-content: space-around">

            <div class="card text-center  shame-card col-4">
                <?= Html::img("@web/images/maillots.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Most given maillot Award</h5>
                    <?= Html::a('View', ['lleva/mgm'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <div class="card text-center  shame-card col-4">
                <?= Html::img("@web/images/B_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Less valued maillot Award</h5>
                    <?= Html::a('View', ['maillot/lva'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Print dossier', ['site/print'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Go back', ['site/shame'], ['class' => 'btn btn-danger']) ?>
        </div>

    </div>
</div>